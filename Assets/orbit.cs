﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class orbit : MonoBehaviour
{
    public float speed; 
    public float radius; 
    public float angle;
    public Transform centerobject;
    public float interpolationspeed;
    void Update()
    {
  
            angle =angle+  speed * Time.deltaTime;

        var quat = Quaternion.Euler(new Vector3(0, angle, 0));

       var unrotated_position = new Vector3(radius, 0, 0);
        var rotated_position = quat * unrotated_position;

        this.transform.position = rotated_position;

        
        this.transform.rotation = Quaternion.Slerp(quat,centerobject.rotation,interpolationspeed);
    }
}
