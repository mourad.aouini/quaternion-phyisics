﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class faceplayer : MonoBehaviour
{
    // Start is called before the first frame update
    public Transform target;

    void Update()
    {
        Vector3 distance = target.position - transform.position;

       
        Quaternion rotation = Quaternion.LookRotation(distance, Vector3.up);
        transform.rotation = rotation;
    }
}
